# -*- coding: utf-8 -*-
"""
Created on Sat Jun 24 00:45:04 2017

@author: Jan
"""
import random
import time
import socket
import json
from builtins import bytes
# import ipaaca
# ipaacaOutbuffer = ipaaca.OutputBuffer("IpaacaTest")

def client(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    sock.sendall(bytes(message + "\n", "utf-8"))
    sock.close()

try:
    while True:
    
        data = {"y": random.random(), # For Line data
                "dist": list(enumerate([random.random() for i in range(5)])),
                "dist2": {c: random.random() for c in "abcde"},
                "y2": [random.random() for i in range(20)],
                "x": ["a","b","c","d"],
                "d": [random.random() for i in range(4)],
                "checked": {m: True if random.random()>0.5 else False for m in ["Expl1", "Expl2", "Expl3"]},
                "nested": {"t": random.random(), "t2": 0.2}
                }

        idx = random.randint(0,4)
        el = data["dist"][idx]
        data["dist"][idx] = (el[0], el[1], "red")
        
        try:
            port = 9080
            client("localhost", port, json.dumps(data))
        except:
            pass

        # msg = ipaaca.Message("test")
        # msg.payload = {k: str(v) for k,v in data.items()}
        # msg.payload["x"] = ["a","b","c","d"]
        # msg.payload["d"] = [random.random() for i in range(4)]
        # ipaacaOutbuffer.add(msg)
        
        time.sleep(0.15)
except KeyboardInterrupt:
    pass
#    del informer
    # del ipaacaOutbuffer
