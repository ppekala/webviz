import {LinePlotStore, LinePlotInformation, BarPlotInformation, BarPlotStore} from "./plots";
import {SliderInformation, CustomSliderStore} from "./slider";
import {CheckedTextStore, CheckedTextInformation} from './connectedCheckedText';
import { ExperimentViewInformation, ExperimentViewStore } from './connectedExperimentView';

import lineThumb from '../img/linePlot.png'
import barThumb from '../img/barPlot.png'
import sliderThumb from '../img/slider.png'
import gridThumb from '../img/gridPlot.png'

var modules = [{id: 1,
    name: "LinePlot",
    img: lineThumb},
    {id: 2,
    name: "BarPlot",
    img: barThumb},
    {id: 3,
    name: "Gridworld",
    img: gridThumb},
    {id: 4,
    name: "Slider",
    img: sliderThumb},
    {id: 5,
    name: "ExperimentView",
    img: gridThumb},
    {id: 6,
    name: "CheckedText",
    img: null}
]


const mapIDToConfig = {1: LinePlotInformation, 2: BarPlotInformation, 4: SliderInformation, 5: ExperimentViewInformation, 6:CheckedTextInformation}
const mapTypeToComponent = {"LinePlot": LinePlotStore, 
                            "BarPlot": BarPlotStore, 
                            "Slider": CustomSliderStore, 
                            "ExperimentView": ExperimentViewStore, 
                            "CheckedText":CheckedTextStore}

export {modules, mapIDToConfig, mapTypeToComponent}